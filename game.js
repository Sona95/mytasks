let winComb = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],

]

let current = "X";
let state = ["", "", "", "", "", "", "", "", "", ];
let isGameActiv = true;







document.querySelectorAll('.cell').forEach(cell => cell.addEventListener('click', cellClick));

function cellClick(myEvent) {
    let cell = myEvent.target;
    let index = cell.getAttribute("id");
    if (state[index] != "" || !isGameActiv) {
        return;
    }
    state[index] = current;
    cell.innerHTML = current;
    checkResult();

}

function checkResult() {
    let won = false;


    for (let i = 0; i < winComb.length; i++) {
        const element = winComb[i];
        if (state[winComb[i][0]] == "" || state[winComb[i][1]] == "" || state[winComb[i][2]] == "") {
            continue;
        }
        if (state[winComb[i][0]] == state[winComb[i][1]] && state[winComb[i][1]] == state[winComb[i][2]]) {
            won = true;
            break;
        }

    }



    if (won) {
        alert(`${current} wins`);
        isGameActiv == false;
        return;
    }

    let draw = !state.includes("");
    if (draw) {
        alert("draw");
        isGameActiv = false;
        return;
    }

    changePlayer();
}

function changePlayer() {
    current = current == "X" ? "O" : "X";
}

function restartGame() {
    gameActive = true;
    current = "X";
    state = ["", "", "", "", "", "", "", "", ""];
    document.querySelectorAll('.cell').forEach(cell => cell.innerHTML = "");
}

document.querySelector('.restart').addEventListener('click', restartGame);